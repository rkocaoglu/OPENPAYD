package com.openpayd.dao;

import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.openpayd.entities.Client;
import com.openpayd.errors.ClientNotFoundException;

@Qualifier("client")
@Repository
public class HibernateClientDAO extends HibernateDAOBase implements IClientDAO {

	@Transactional
	public Client addClient(Client client) {
		Session session = getEntityManager().unwrap(Session.class);
		session.save(client);
		return client;
	}

	@Transactional
	public void deleteClient(Client client) {
		Session session = getEntityManager().unwrap(Session.class);
		session.delete(client);
	}

	@Transactional
	public void updateClient(Client client) {
		Session session = getEntityManager().unwrap(Session.class);
		session.update(client);
	}

	@Transactional
	public List<Client> getAllClients() {
		Session session = getEntityManager().unwrap(Session.class);
		List<Client> clients = session.createQuery("from Client", Client.class).getResultList();
		if (clients == null || clients.size() == 0)
			throw new ClientNotFoundException();
		return clients;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public Client getClientByClientId(long clientId) {
		Session session = getEntityManager().unwrap(Session.class);
		Query query = session.createQuery("from Client c where c.clientid=:clientId", Client.class);
		query.setParameter("clientId", clientId);
		List<Client> clients = (List<Client>) query.getResultList();
		if (clients.size() > 0) {
			return clients.get(0);
		} else {
			throw new ClientNotFoundException(clientId);
		}
	}

}
