package com.openpayd.dao;

import java.util.List;

import com.openpayd.entities.Transaction;

public interface ITransactionDAO {
	public Transaction addTransaction(Transaction transaction);

	public void deleteTransaction(Transaction transaction);

	public Transaction updateTransaction(Transaction transaction);

	public List<Transaction> getAllTransactions();

	public Transaction getTransactionByTransactionId(long transactionId);
}
