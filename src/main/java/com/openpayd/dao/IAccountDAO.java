package com.openpayd.dao;

import java.util.List;

import com.openpayd.entities.Account;

public interface IAccountDAO {
	public Account addAccount(Account account);

	public void deleteAccount(Account account);

	public void updateAccout(Account account);

	public List<Account> getAllAccounts();

	public Account getAccountByAccountId(long accountId);
}
