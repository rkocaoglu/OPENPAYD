package com.openpayd.dao;

import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.openpayd.entities.Transaction;

@Qualifier("transaction")
@Repository
public class HibernateTransactionDAO extends HibernateDAOBase implements ITransactionDAO {

	@Transactional
	public Transaction addTransaction(Transaction transaction) {
		Session session = getEntityManager().unwrap(Session.class);
		session.save(transaction);
		return transaction;
	}

	@Transactional
	public void deleteTransaction(Transaction transaction) {
		Session session = getEntityManager().unwrap(Session.class);
		session.delete(transaction);
	}

	@Transactional
	public Transaction updateTransaction(Transaction transaction) {
		Session session = getEntityManager().unwrap(Session.class);
		session.update(transaction);
		return transaction;
	}

	@Transactional
	public List<Transaction> getAllTransactions() {
		Session session = getEntityManager().unwrap(Session.class);
		List<Transaction> transactions = session.createQuery("from Transaction", Transaction.class).getResultList();
		return transactions;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public Transaction getTransactionByTransactionId(long transactionId) {
		Session session = getEntityManager().unwrap(Session.class);
		Query query = session.createQuery("from Transaction t where t.transactionid=:transactionId", Transaction.class);
		query.setParameter("transactionId", transactionId);
		List<Transaction> transactions = (List<Transaction>) query.getResultList();
		if (transactions.size() > 0) {
			return transactions.get(0);
		} else {
			return null;
		}
	}

}
