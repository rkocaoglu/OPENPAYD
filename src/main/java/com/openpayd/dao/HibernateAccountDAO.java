package com.openpayd.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.openpayd.entities.Account;
import com.openpayd.errors.AccountNotFoundException;

@Qualifier("account")
@Repository
public class HibernateAccountDAO extends HibernateDAOBase implements IAccountDAO {

	@Transactional
	public Account addAccount(Account account) {
		Session session = getEntityManager().unwrap(Session.class);
		session.save(account);
		return account;
	}

	@Transactional
	public void deleteAccount(Account account) {
		Session session = getEntityManager().unwrap(Session.class);
		session.delete(account);
	}

	@Transactional
	public void updateAccout(Account account) {
		Session session = getEntityManager().unwrap(Session.class);
		session.update(account);
	}

	@Transactional
	public List<Account> getAllAccounts() {
		Session session = getEntityManager().unwrap(Session.class);
		List<Account> accounts = session.createQuery("from Account", Account.class).getResultList();
		if (accounts == null || accounts.size() == 0)
			throw new AccountNotFoundException();
		return accounts;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public Account getAccountByAccountId(long accountId) {
		Session session = getEntityManager().unwrap(Session.class);
		Query query = session.createQuery("from Account a where a.accountid=:accountId", Account.class);
		query.setParameter("accountId", accountId);
		List<Account> accounts = (List<Account>) query.getResultList();
		if (accounts.size() > 0) {
			return accounts.get(0);
		} else {
			throw new AccountNotFoundException(accountId);
		}
	}

}
