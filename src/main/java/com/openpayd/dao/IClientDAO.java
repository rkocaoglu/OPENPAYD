package com.openpayd.dao;

import java.util.List;
import com.openpayd.entities.Client;

public interface IClientDAO {

	public Client addClient(Client client);

	public void deleteClient(Client client);

	public void updateClient(Client client);

	public List<Client> getAllClients();

	public Client getClientByClientId(long clientId);

}
