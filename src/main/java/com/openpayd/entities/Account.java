package com.openpayd.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.openpayd.enums.BalanceStatus;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the accounts database table.
 * 
 */
@Entity
@Table(name = "accounts", indexes = { @Index(name = "indx_accounts_accountid", columnList = "accountid", unique = true),
		@Index(name = "indx_accounts_clientid", columnList = "clientid", unique = false) })
@NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a")
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique = true, nullable = false, precision = 15)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "accountid_generator")
	@SequenceGenerator(name = "accountid_generator", sequenceName = "ACCOUNTS_ACCOUNTID_SEQ", allocationSize = 1)
	private long accountid;

	private String accounttype;

	private BigDecimal balance;

	private String balancestatus;

	private Timestamp creationdate;

	private BigDecimal interestrate;

	public BigDecimal getInterestrate() {
		return interestrate;
	}

	public void setInterestrate(BigDecimal interestrate) {
		this.interestrate = interestrate;
	}

	// bi-directional many-to-one association to Client
	@JsonBackReference(value = "accounts")
	@ManyToOne
	@JoinColumn(name = "clientid")
	private Client client;

	// bi-directional many-to-one association to Transaction
	@JsonManagedReference(value = "debitTransactions")
	@OneToMany(mappedBy = "debitAccount", cascade = CascadeType.ALL)
	private List<Transaction> debitTransactions;

	// bi-directional many-to-one association to Transaction
	@JsonManagedReference(value = "creditTransactions")
	@OneToMany(mappedBy = "creditAccount", cascade = CascadeType.ALL)
	private List<Transaction> creditTransactions;

	public Account() {
	}

	public long getAccountid() {
		return this.accountid;
	}

	public void setAccountid(long accountid) {
		this.accountid = accountid;
	}

	public String getAccounttype() {
		return this.accounttype;
	}

	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getBalancestatus() {
		return this.balancestatus;
	}

	public void setBalancestatus(String balancestatus) {
		this.balancestatus = balancestatus;
	}

	public Timestamp getCreationdate() {
		return this.creationdate;
	}

	public void setCreationdate(Timestamp creationdate) {
		this.creationdate = creationdate;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Transaction> getDebitTransactions() {
		return this.debitTransactions;
	}

	public void setDebitTransactions(List<Transaction> debitTransactions) {
		this.debitTransactions = debitTransactions;
	}

	public void addDebitTransactions(Transaction debitTransaction) {
		debitTransaction.setDebitAccount(this);
		getDebitTransactions().add(debitTransaction);

	}

	public void removeDebitTransactions(Transaction debitTransaction) {
		getDebitTransactions().remove(debitTransaction);

	}

	public List<Transaction> getCreditTransactions() {
		return this.creditTransactions;
	}

	public void setCreditTransactions(List<Transaction> creaditTransactions) {
		this.creditTransactions = creaditTransactions;
	}

	public void addCreditTransactions(Transaction creditTransactions) {
		getCreditTransactions().add(creditTransactions);
	}

	public void removeCreditTransactions(Transaction creditTransactions) {
		getCreditTransactions().remove(creditTransactions);

	}

	public void debit(BigDecimal amount) {
		if (this.getBalancestatus().equals(BalanceStatus.CREDIT.getBalanceStatusKod())) {
			if (this.balance.compareTo(amount) == 0) {
				this.balance = this.balance.subtract(amount);
			}
			if (this.balance.compareTo(amount) == 1) {
				this.balance = this.balance.subtract(amount);
			}
			if (this.balance.compareTo(amount) == -1) {
				this.balance = this.balance.subtract(amount);
				this.balancestatus = BalanceStatus.DEBIT.getBalanceStatusKod();
			}

		}
		if (this.getBalancestatus().equals(BalanceStatus.DEBIT.getBalanceStatusKod())) {
			this.balance = this.balance.subtract(amount);
		}

	}

	public void credit(BigDecimal amount) {
		if (this.getBalancestatus().equals(BalanceStatus.CREDIT.getBalanceStatusKod())) {
			this.balance = this.balance.add(amount);
		}
		if (this.getBalancestatus().equals(BalanceStatus.DEBIT.getBalanceStatusKod())) {
			if (this.balance.abs().compareTo(amount) == 0) {
				this.balance = this.balance.add(amount);
				this.setBalancestatus(BalanceStatus.CREDIT.getBalanceStatusKod());
			}
			if (this.balance.abs().compareTo(amount) == 1) {
				this.balance = this.balance.add(amount);
			}
			if (this.balance.abs().compareTo(amount) == -1) {
				this.balance = amount.add(this.balance);
				this.balancestatus = BalanceStatus.CREDIT.getBalanceStatusKod();
			}

		}

	}

}