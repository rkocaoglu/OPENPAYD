package com.openpayd.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * The persistent class for the addresses database table.
 * 
 */
@Entity
@Table(name = "addresses", indexes = {
		@Index(name = "indx_addresses_accountid", columnList = "addressid", unique = true),
		@Index(name = "indx_addresses_clientid", columnList = "clientid", unique = false) })
@NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a")
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique = true, nullable = false, precision = 13)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "addressid_generator")
	@SequenceGenerator(name = "addressid_generator", sequenceName = "ADDRESSES_ADDRESSID_SEQ", allocationSize = 1)
	private long addressid;

	private String addressline1;

	private String addressline2;

	private String addresstype;

	private String city;

	private String country;

	// bi-directional many-to-one association to Client
	@JsonBackReference(value = "addresses")
	@ManyToOne
	@JoinColumn(name = "clientid")
	private Client client;

	public Address() {
	}

	public long getAddressid() {
		return this.addressid;
	}

	public void setAddressid(long addressid) {
		this.addressid = addressid;
	}

	public String getAddressline1() {
		return this.addressline1;
	}

	public void setAddressline1(String addressline1) {
		this.addressline1 = addressline1;
	}

	public String getAddressline2() {
		return this.addressline2;
	}

	public void setAddressline2(String addressline2) {
		this.addressline2 = addressline2;
	}

	public String getAddresstype() {
		return this.addresstype;
	}

	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}