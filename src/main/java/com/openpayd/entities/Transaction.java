package com.openpayd.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * The persistent class for the transactions database table.
 * 
 */
@Entity
@Table(name = "transactions", indexes = {
		@Index(name = "indx_transactions_transactionid", columnList = "transactionid", unique = true),
		@Index(name = "indx_transactions_debitaccountid", columnList = "debitaccountid", unique = false),
		@Index(name = "indx_transactions_creditaccountid", columnList = "creditaccountid", unique = false) })
@NamedQuery(name = "Transaction.findAll", query = "SELECT t FROM Transaction t")
public class Transaction implements Serializable {
	private static final long serialVersionUID = 1L;

	public Transaction(Account debitAccount, Account creditAccount, BigDecimal amount, String message) {
		this.debitAccount = debitAccount;
		this.creditAccount = creditAccount;
		this.amount = amount;
		this.message = message;
		Date date = new Date();
		this.transactiondate = new Timestamp(date.getTime());
	}

	@Id
	@Column(unique = true, nullable = false, precision = 15)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transactionId_generator")
	@SequenceGenerator(name = "transactionId_generator", sequenceName = "TRANSACTIONS_TRANSACTIONID_SEQ", allocationSize = 1)
	private long transactionid;

	private BigDecimal amount;

	private String message;

	private Timestamp transactiondate;

	// bi-directional many-to-one association to Account
	@JsonBackReference(value = "creditTransactions")
	@ManyToOne
	@JoinColumn(name = "creditaccountid")
	private Account creditAccount;

	// bi-directional many-to-one association to Account
	@JsonBackReference(value = "debitTransactions")
	@ManyToOne
	@JoinColumn(name = "debitaccountid")
	private Account debitAccount;

	public Transaction() {
	}

	public long getTransactionid() {
		return this.transactionid;
	}

	public void setTransactionid(long transactionid) {
		this.transactionid = transactionid;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getTransactiondate() {
		return this.transactiondate;
	}

	public void setTransactiondate(Timestamp transactiondate) {
		this.transactiondate = transactiondate;
	}

	public Account getCreditAccount() {
		return this.creditAccount;
	}

	public void setCreditAccount(Account creditAccount) {
		this.creditAccount = creditAccount;
	}

	public Account getDebitAccount() {
		return this.debitAccount;
	}

	public void setDebitAccount(Account debitAccount) {
		this.debitAccount = debitAccount;
	}

}