package com.openpayd.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

/**
 * The persistent class for the clients database table.
 * 
 */
@Entity
@Table(name = "clients", indexes = { @Index(name = "indx_clients_clientid", columnList = "clientid", unique = true) })
@NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique = true, nullable = false, precision = 13)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "clientid_generator")
	@SequenceGenerator(name = "clientid_generator", sequenceName = "CLIENTS_CLIENTID_SEQ", allocationSize = 1)
	private long clientid;

	private String name;

	private String surname;

	// bi-directional many-to-one association to Account
	@JsonManagedReference(value = "accounts")
	@OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
	private List<Account> accounts;

	@JsonManagedReference(value = "addresses")
	@OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
	private List<Address> addresses;

	public Client() {
	}

	public long getClientid() {
		return this.clientid;
	}

	public void setClientid(long clientid) {
		this.clientid = clientid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<Account> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public Account addAccount(Account account) {
		getAccounts().add(account);
		account.setClient(this);

		return account;
	}

	public Account removeAccount(Account account) {
		getAccounts().remove(account);
		account.setClient(null);

		return account;
	}

	public List<Address> getAddresses() {
		return this.addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public Address addAddress(Address address) {
		getAddresses().add(address);
		address.setClient(this);

		return address;
	}

	public Address removeAddress(Address address) {
		getAddresses().remove(address);
		address.setClient(null);

		return address;
	}

}