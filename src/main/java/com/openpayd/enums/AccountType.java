package com.openpayd.enums;

public enum AccountType {
	CURRENT("C"), SAVINGS("S");

	private String accountType;

	AccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountType() {
		return accountType;
	}

	public static AccountType findByAccountType(String accountType) {
		for (AccountType aType : AccountType.values()) {
			if (aType.getAccountType().equals(accountType)) {
				return aType;
			}
		}
		return null;
	}
}
