package com.openpayd.enums;

public enum BalanceStatus {
	DEBIT("DR"), CREDIT("CR");

	private String balanceStatusKod;

	BalanceStatus(String balanceStatusKod) {
		this.balanceStatusKod = balanceStatusKod;
	}

	public String getBalanceStatusKod() {
		return balanceStatusKod;
	}

	public static BalanceStatus findByBalanceStatusKod(String balanceStatusKod) {
		for (BalanceStatus balanceStatus : BalanceStatus.values()) {
			if (balanceStatus.getBalanceStatusKod().equals(balanceStatusKod)) {
				return balanceStatus;
			}
		}
		return null;
	}

}
