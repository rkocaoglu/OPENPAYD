package com.openpayd.dvo;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class TransferDVO {
	private long creditAccountid;
	private long debitAccountid;
	private BigDecimal amount;
	private String message;
	private Timestamp transactionDate;

	public long getCreditAccountid() {
		return creditAccountid;
	}

	public void setCreditAccountid(long creditAccountid) {
		this.creditAccountid = creditAccountid;
	}

	public long getDebitAccountid() {
		return debitAccountid;
	}

	public void setDebitAccountid(long debitAccountid) {
		this.debitAccountid = debitAccountid;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

}
