package com.openpayd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openpayd.dao.IAccountDAO;
import com.openpayd.dao.IClientDAO;
import com.openpayd.dao.ITransactionDAO;
import com.openpayd.dvo.TransferDVO;
import com.openpayd.entities.Account;
import com.openpayd.entities.Client;
import com.openpayd.entities.Transaction;
import com.openpayd.errors.AccountHasNotAnyTransaction;
import com.openpayd.errors.ClientHasNotAnyAccountException;
import com.openpayd.errors.ClientNotFoundException;
import com.openpayd.errors.FailedTranfer;

@Service
public class BankService implements IBankService {

	@Autowired
	private IClientDAO clientDAO;
	@Autowired
	private IAccountDAO accountDAO;
	@Autowired
	private ITransactionDAO transactionDAO;

	@Override
	@Transactional
	public List<Client> getAllClients() {
		return clientDAO.getAllClients();
	}

	@Override
	@Transactional
	public Client getClientByClientId(long clientid) {
		if (clientDAO.getClientByClientId(clientid) == null) {
			throw new ClientNotFoundException(clientid);
		}

		return clientDAO.getClientByClientId(clientid);
	}

	@Override
	@Transactional
	public Client addClient(Client client) {
		return clientDAO.addClient(client);
	}

	@Override
	@Transactional
	public Account addAccount(Account account) {
		return accountDAO.addAccount(account);
	}

	@Override
	@Transactional
	public Transaction transfer(TransferDVO transferDVO) {
		Account debitAccount = accountDAO.getAccountByAccountId(transferDVO.getDebitAccountid());
		if (debitAccount == null)
			throw new FailedTranfer("The Debit account(" + transferDVO.getDebitAccountid()
					+ ") is not found.The tranfer operation failed.");
		Account creditAccount = accountDAO.getAccountByAccountId(transferDVO.getCreditAccountid());
		if (creditAccount == null)
			throw new FailedTranfer("The Credit account(" + transferDVO.getCreditAccountid()
					+ ") is not found.The tranfer operation failed.");
		debitAccount.debit(transferDVO.getAmount());
		creditAccount.credit(transferDVO.getAmount());
		Transaction transaction = new Transaction(debitAccount, creditAccount, transferDVO.getAmount(),
				transferDVO.getMessage());
		accountDAO.updateAccout(debitAccount);
		accountDAO.updateAccout(creditAccount);
		transactionDAO.addTransaction(transaction);
		if (transaction.getTransactionid() == 0)
			throw new FailedTranfer("The tranfer operation failed.");

		return transaction;

	}

	@Override
	public List<Account> getClientAccounts(long clientid) {
		Client client = clientDAO.getClientByClientId(clientid);
		if (client.getAccounts() == null || client.getAccounts().size() == 0)
			throw new ClientHasNotAnyAccountException();
		return client.getAccounts();
	}

	@Override
	public List<Transaction> getAccountTransactions(long accountid) {
		Account account = accountDAO.getAccountByAccountId(accountid);
		List<Transaction> transactionList = account.getCreditTransactions();
		transactionList.addAll(account.getDebitTransactions());
		if (transactionList == null || transactionList.size() == 0)
			throw new AccountHasNotAnyTransaction();

		return transactionList;
	}

}
