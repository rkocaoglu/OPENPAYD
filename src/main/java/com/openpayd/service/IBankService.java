package com.openpayd.service;

import java.util.List;

import com.openpayd.dvo.TransferDVO;
import com.openpayd.entities.Account;
import com.openpayd.entities.Client;
import com.openpayd.entities.Transaction;

public interface IBankService {

	List<Client> getAllClients();

	Client getClientByClientId(long clientid);

	Client addClient(Client client);
	
	Account addAccount(Account account);
	
	Transaction transfer(TransferDVO transferDVO);

	List<Account> getClientAccounts(long clientid);

	List<Transaction> getAccountTransactions(long accountid);



}
