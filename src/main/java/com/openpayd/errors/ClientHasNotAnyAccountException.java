package com.openpayd.errors;

public class ClientHasNotAnyAccountException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClientHasNotAnyAccountException() {
		super("Client has not any account!");
	}

}
