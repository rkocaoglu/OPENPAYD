package com.openpayd.errors;

public class AccountHasNotAnyTransaction extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AccountHasNotAnyTransaction() {
		super("Account has not any Transaction : ");
	}
}
