package com.openpayd.errors;

public class FailedTranfer extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FailedTranfer(String message) {
		super(message);
	}

}
