package com.openpayd.errors;

public class ClientNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClientNotFoundException(long clientid) {
		super("Clientid not found : " + clientid);
	}

	public ClientNotFoundException() {
		super("Any Client is not found : ");
	}
}
