package com.openpayd.errors;

public class AccountNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotFoundException(long accountid) {
		super("Accountid not found : " + accountid);
	}

	public AccountNotFoundException() {
		super("Any Account is not found : ");
	}
}
