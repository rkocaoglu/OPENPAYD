package com.openpayd.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.openpayd.dvo.TransferDVO;
import com.openpayd.entities.Account;
import com.openpayd.entities.Client;
import com.openpayd.entities.Transaction;
import com.openpayd.service.IBankService;

@RestController()
@RequestMapping(path = "/bank/api")
public class BankController {

	@Autowired
	private IBankService bankService;

	@PostMapping("/clients")
	Client newClient(@RequestBody Client client) {
		return bankService.addClient(client);
	}

	@GetMapping("/clients")
	public List<Client> getAllClients() {
		return bankService.getAllClients();
	}

	@GetMapping("/clients/{clientid}")
	public Client getClient(@PathVariable long clientid) {
		return bankService.getClientByClientId(clientid);
	}

	@PostMapping("/transfer")
	public Transaction tranfer(@RequestBody TransferDVO transferdvo) {
		return bankService.transfer(transferdvo);
	}

	@GetMapping("/clients/{clientid}/accounts")
	public List<Account> getClientAccounts(@PathVariable long clientid) {
		return bankService.getClientAccounts(clientid);
	}

	@GetMapping("/accounts/{accountid}/transactions")
	public List<Transaction> getAccountTransactions(@PathVariable long accountid) {
		return bankService.getAccountTransactions(accountid);
	}
}
