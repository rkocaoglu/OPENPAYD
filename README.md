I followed this steps when I was developing the project:

1-Creation of the Project
2-Open Project on Eclipse IDE
3- Package Structure of  the Project
4-Set PostgreSQL Database Settings In application.properties.
5-Designing Class Diagram
6-Database Table Structure
7-Transaction Management Using @Transactional Anotation
8-Exception Handlig Mechanisim
9-Spring Boot – Deploy WAR file to Tomcat
10-Test Cases


You can find the details of these steps in the document on this link. [Development Document](documents/DevelopmentDocument.docx)